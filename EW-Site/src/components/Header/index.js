import React from 'react'
import Link from 'gatsby-link'

const Header = () => (
  <div id='header'>
    {/* <div
      // style={{
      //   margin: '0 auto',
      //   maxWidth: 960,
      //   padding: '1.45rem 1.0875rem',
      // }}
    > */}
      <div style={{ margin: 0 }}>
        <Link
          to="/"
          style={{
            color: 'black',
            textDecoration: 'none',
            'font-family': 'Raleway',
          }}
        >
          EdWhe.com
        </Link>
      </div>
      
      <div><Link to="/projects" id='push'>Projects</Link></div>
      <div><Link to="/contact">Contact</Link></div>
    
    
    </div>

    // <div id='footer'>1</div>
  // </div>
)

export default Header
