import React from 'react'
import Link from 'gatsby-link'
import GitlabLogo from './gitlab-logo.png'

const Footer = () => (
  <div id='footer'>
  <a href='https://gitlab.com/edwhelan'> 
  <img src={GitlabLogo} alt='GitLab'/>
  </a>
    </div>

 
)

export default Footer
