import React from 'react'
import Link from 'gatsby-link'

const ContactPage = () => (
  <div id='contactBox'>
    <div id='contactText'>
    <div>
    <p><b>Feel free to reach out and contact me: <a href="mailto:vroxing@gmail.com">vroxing@gmail.com</a></b></p><br/>
   <p> “Where now are the horse and the rider? Where is the horn that was blowing?
Where is the helm and the hauberk, and the bright hair flowing?
Where is the harp on the harpstring, and the red fire glowing?
Where is the spring and the harvest and the tall corn growing?
They have passed like rain on the mountain, like a wind in the meadow;
The days have gone down in the West behind the hills into shadow.
Who shall gather the smoke of the deadwood burning,
Or behold the flowing years from the Sea returning?” 
― J.R.R. Tolkien</p>
  </div>
  </div>
  </div>
)

export default ContactPage
