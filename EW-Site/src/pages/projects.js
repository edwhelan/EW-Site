import React from 'react'
import Link from 'gatsby-link'
import rareCap from './rareCapsuleProj.jpg'

const ProjectsPage = () => (
  <div id='projectBox'>
    <div id='projectText'>
    <h1>Projects:</h1>
    <p>A list of some of my projects.</p>
    <p>rarecapsule.com</p>
    <div id="container-a2">
		<ul id="caption-style-2">
			<li>
				<a href='http://www.rarecapsule.com'><img src={rareCap} alt="RAREcapsule.com"/>
				<div id="caption">
					<div id="blur"></div>
					<div id="caption-text">
						<h1>rarecapsule.com</h1>
						<p>portfolio site for Tim Glover<br/>Made with Gatsbyjs</p>
					</div>
				</div>
				</a>
			</li>
      </ul>
      </div>
  </div>
  </div>
)

export default ProjectsPage
