import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div id='indexBox'>

    <div id="indexText">
      <h1>Im Edward,</h1>
      <p>I like Baseball, Coffee, && building things.</p>
      <p>My life was changed when I learned that anything I wanted in life was achievable through practice and determination. Since then I've never been more focused in my life. I have always been a fan of design and functionality, I like to pass those things into everything I do. Please check out my projects and if you have any questions or criticism's please reach out. </p>
      
    </div>
  </div>
)

export default IndexPage
